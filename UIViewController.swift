//
//  UIViewController.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-24.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

extension UIViewController{
    
    ///Make sure storyboard file has same name as the class name
    public static func storyboardInstance() -> UIViewController?{
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        let controller = storyboard.instantiateInitialViewController()
        return controller
    }
    
    public func present(controller: UIViewController?){
        
        guard let controller = controller else{
            return
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    ///Adds tap gesture recognizer to root view that dismissess keyboard on tap
    public func addDismissKeyboardOnTapRecognizer(on view: UIView){
        let tapRec = UITapGestureRecognizer()
        tapRec.cancelsTouchesInView = false
        tapRec.addTarget(self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapRec)
    }
    
    public func dismissKeyboard(){
        view.endEditing(true)
    }
    
    public func push(controller: UIViewController?){
        guard let controller = controller else { return }
        navigationController?.pushViewController(controller, animated: true)
    }
    
}


extension UIViewController{
    
    public func addObserver(_ selector: Selector,_ name: String,_ object: AnyObject?=nil){
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name(rawValue: name), object: object)
    }
    
    public func addKeyboardDidShowNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    public func addKeyboardWillShowNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    public func addKeyboardWillHideNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    open func keyboardDidShow(_ notification: Notification) {}
    
    open func keyboardWillHide(_ notification: Notification) {}
    
    open func keyboardWillShow(_ notification: Notification) {}
    
    open func removeObservers(){
        NotificationCenter.default.removeObserver(self)
    }
}

extension UIViewController{
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
    }
    
}
