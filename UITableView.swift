//
//  UITableView.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-24.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

extension UITableView{
    
    public func register(nibName:String){
        let nib = UINib(nibName: nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: nibName)
    }
    
    
    public func dequeue(identifier: String) -> UITableViewCell?{
        return dequeueReusableCell(withIdentifier: identifier)
    }
    
}
