//
//  Conversations.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK

class Conversations: UITableViewCell {
    
    //MARK: Variables
    
    @IBOutlet weak var dateL: UILabel!
    @IBOutlet weak var subjectL: UILabel!
    @IBOutlet weak var starterL: UILabel!
    @IBOutlet weak var onlineIndicator: UIView!
    @IBOutlet weak var leftLineView: UIView!
    
}

//MARK: UITableViewCell Methods
extension Conversations{
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    static func cellReuseIdentifier() -> String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        onlineIndicator.isHidden = true
        
    }
    
}

//MARK: Set Methods
extension Conversations{
    
    func setRow(row: NSInteger) {
        switch (row % 5) {
        case 0:
            self.leftLineView.backgroundColor = Constants.meetChannelLineColorNo0()
            break;
            
        case 1:
            self.leftLineView.backgroundColor = Constants.meetChannelLineColorNo1()
            break;
            
        case 2:
            self.leftLineView.backgroundColor = Constants.meetChannelLineColorNo2()
            break;
            
        case 3:
            self.leftLineView.backgroundColor = Constants.meetChannelLineColorNo3()
            break;
            
        case 4:
            self.leftLineView.backgroundColor = Constants.meetChannelLineColorNo4()
            break;
            
        default:
            self.leftLineView.backgroundColor = Constants.meetChannelLineColorNo0()
            break;
        }
    }
    
    func setConversation(subject: String, starter: String) {
        
        subjectL.text = subject
        starterL.text = starter
        
    }
    
    func checkIfOnline (user: SBDUser){
        
        if user.connectionStatus == .online{
            onlineIndicator.isHidden = false
            
        }
        
    }
    
}
