//
//  ChatListViewController.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import MGSwipeTableCell
import SendBirdSDK
import RZTransitions

class ChatListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate, SBDChannelDelegate, SBDConnectionDelegate {
    
    //MARK: Variables
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noChannelLabel: UILabel!
    
    fileprivate var refreshControl: UIRefreshControl?
    fileprivate var channels: [SBDGroupChannel] = []
    fileprivate var editableChannel: Bool = false
    fileprivate var groupChannelListQuery: SBDGroupChannelListQuery?
    fileprivate var typingAnimationChannelList: [String] = []

    fileprivate var cachedChannels: Bool = true
    
    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
    
}

//MARK: UIViewController Methods
extension ChatListViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RZTransitionsManager.shared().setAnimationController(RZZoomPushAnimationController(), fromViewController: type(of: self), for: .dismiss)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isEditing = false
        self.tableView.register(ChatListTableViewCell.nib(), forCellReuseIdentifier: ChatListTableViewCell.cellReuseIdentifier())
        self.tableView.register(ChatListEditableTableViewCell.nib(), forCellReuseIdentifier: ChatListEditableTableViewCell.cellReuseIdentifier())
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refreshChannelList), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        self.setDefaultNavigationItems()
        
        self.noChannelLabel.isHidden = true
        
        let dumpLoadQueue: DispatchQueue = DispatchQueue(label: "com.sendbird.dumploadqueue", attributes: .concurrent)
        dumpLoadQueue.async {
            self.channels = Utils.loadGroupChannels()
            if self.channels.count > 0 {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(150), execute: {
                        self.refreshChannelList()
                    })
                }
            }
            else {
                self.cachedChannels = false
                self.refreshChannelList()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Utils.dumpChannels(channels: self.channels)
    }
}

//MARK: AddDelegates Method
extension ChatListViewController{
    
    func addDelegates() {
        SBDMain.add(self as SBDChannelDelegate, identifier: self.description)
        SBDMain.add(self as SBDConnectionDelegate, identifier: self.description)
    }
    
}

//MARK: Setup Methods
extension ChatListViewController{
    
    fileprivate func setDefaultNavigationItems() {
        let negativeLeftSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeLeftSpacer.width = -2
        let negativeRightSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeRightSpacer.width = -2
        
        let leftBackItem = UIBarButtonItem(image: UIImage(named: "btn_back"), style: UIBarButtonItemStyle.done, target: self, action: #selector(back))
        let rightEditItem = UIBarButtonItem(image: UIImage(named: "btn_edit"), style: UIBarButtonItemStyle.done, target: self, action: #selector(editGroupChannel))
        rightEditItem.imageInsets = UIEdgeInsetsMake(0, 14, 0, -14)
        
        self.navItem.leftBarButtonItems = [negativeLeftSpacer, leftBackItem]
        self.navItem.rightBarButtonItems = [negativeRightSpacer, rightEditItem]
    }
    
    fileprivate func setEditableNavigationItems() {
        let negativeLeftSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeLeftSpacer.width = -2
        
        let leftBackItem = UIBarButtonItem(title: Bundle.sbLocalizedStringForKey(key: "DoneButton"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(done))
        leftBackItem.setTitleTextAttributes([NSFontAttributeName: Constants.navigationBarButtonItemFont()], for: UIControlState.normal)
        
        self.navItem.leftBarButtonItems = [negativeLeftSpacer, leftBackItem]
        self.navItem.rightBarButtonItems = []
    }
    
}

//MARK: Refresh Target Methods
extension ChatListViewController{
    
    @objc fileprivate func refreshChannelList() {
        self.groupChannelListQuery = SBDGroupChannel.createMyGroupChannelListQuery()
        self.groupChannelListQuery?.limit = 20
        self.groupChannelListQuery?.order = SBDGroupChannelListOrder.latestLastMessage
        
        self.groupChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.refreshControl?.endRefreshing()
                }
                
                return
            }
            
            self.channels.removeAll()
            self.cachedChannels = false
            
            for channel in channels! {
                self.channels.append(channel)
            }
            
            DispatchQueue.main.async {
                if self.channels.count == 0 {
                    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                    self.noChannelLabel.isHidden = false
                }
                else {
                    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                    self.noChannelLabel.isHidden = true
                }
                
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        })
    }
}

//MARK: Target Methods
extension ChatListViewController{
    
    @objc fileprivate func back() {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc fileprivate func editGroupChannel() {
        self.editableChannel = true
        self.setEditableNavigationItems()
        self.tableView.reloadData()
    }
    
}

//MARK: Miscellaneous
extension ChatListViewController{
    
    fileprivate func loadChannels() {
        if self.cachedChannels == true {
            return
        }
        
        if self.groupChannelListQuery != nil {
            if self.groupChannelListQuery?.hasNext == false {
                return
            }
            
            self.groupChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
                if error != nil {
                    DispatchQueue.main.async {
                        self.refreshControl?.endRefreshing()
                    }
                    
                    let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                    let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                    vc.addAction(closeAction)
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                for channel in channels! {
                    self.channels.append(channel)
                }
                
                DispatchQueue.main.async {
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    func done() {
        self.editableChannel = false
        self.setDefaultNavigationItems()
        self.tableView.reloadData()
    }
    
}
    
// MARK: UITableViewDelegate
extension ChatListViewController{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if self.editableChannel == false {
            let vc = ChatChattingViewController(nibName: "ChatChattingViewController", bundle: Bundle.main)
            vc.groupChannel = self.channels[indexPath.row]
            
            self.transitioningDelegate = RZTransitionsManager.shared()
            vc.transitioningDelegate = RZTransitionsManager.shared()
            
            self.present(vc, animated: true, completion: nil)
            
        }
        else {
            let cell = tableView.cellForRow(at: indexPath) as! MGSwipeTableCell
            cell.showSwipe(MGSwipeDirection.rightToLeft, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.loadFromNibWithName(nibNamed: "ChatHeaderView")
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.width, height: 50))
        return view
        
    }
    
}

// MARK: UITableViewDataSource
extension ChatListViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if self.editableChannel == true {
            cell = tableView.dequeueReusableCell(withIdentifier: ChatListEditableTableViewCell.cellReuseIdentifier()) as! ChatListEditableTableViewCell?
            let leaveButton = MGSwipeButton(title: Bundle.sbLocalizedStringForKey(key: "LeaveButton"), backgroundColor: Constants.leaveButtonColor())
            let hideButton = MGSwipeButton(title: Bundle.sbLocalizedStringForKey(key: "HideButton"), backgroundColor: Constants.hideButtonColor())
            
            hideButton.titleLabel?.font = Constants.hideButtonFont()
            leaveButton.titleLabel?.font = Constants.leaveButtonFont()
            
            (cell as! ChatListEditableTableViewCell).rightButtons = [hideButton, leaveButton]
            (cell as! ChatListEditableTableViewCell).setModel(aChannel: self.channels[indexPath.row])
            (cell as! ChatListEditableTableViewCell).delegate = self
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: ChatListTableViewCell.cellReuseIdentifier()) as! ChatListTableViewCell?
            if self.channels[indexPath.row].isTyping() == true {
                if self.typingAnimationChannelList.index(of: self.channels[indexPath.row].channelUrl) == nil {
                    self.typingAnimationChannelList.append(self.channels[indexPath.row].channelUrl)
                }
            }
            else {
                if self.typingAnimationChannelList.index(of: self.channels[indexPath.row].channelUrl) != nil {
                    self.typingAnimationChannelList.remove(at: self.typingAnimationChannelList.index(of: self.channels[indexPath.row].channelUrl)!)
                }
            }
            
            (cell as! ChatListTableViewCell).setModel(aChannel: self.channels[indexPath.row])
        }
        
        if self.channels.count > 0 && indexPath.row + 1 == self.channels.count {
            self.loadChannels()
        }
        
        return cell!
    }
    
}
    
// MARK: MGSwipeTableCellDelegate
extension ChatListViewController{
    
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        // 0: right, 1: left
        let row = self.tableView.indexPath(for: cell)?.row
        let selectedChannel: SBDGroupChannel = self.channels[row!] as SBDGroupChannel
        if index == 0 {
            // Hide
            selectedChannel.hide(completionHandler: { (error) in
                if error != nil {
                    let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                    let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                    vc.addAction(closeAction)
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                if let index = self.channels.index(of: selectedChannel) {
                    self.channels.remove(at: index)
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            })
        }
        else {
            // Leave
            selectedChannel.leave(completionHandler: { (error) in
                if error != nil {
                    let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                    let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                    vc.addAction(closeAction)
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                if let index = self.channels.index(of: selectedChannel) {
                    self.channels.remove(at: index)
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            })
        }
        
        return true
    }
    
}
    
// MARK: CreateGroupChannelUserListViewControllerDelegate
extension ChatListViewController{
    
    func openGroupChannel(channel: SBDGroupChannel, vc: UIViewController) {
        DispatchQueue.main.async {
            let vc = ChatChattingViewController(nibName: "ChatChattingViewController", bundle: Bundle.main)
            vc.groupChannel = channel
            self.present(vc, animated: false, completion: nil)
        }
    }
    
}
    
// MARK: GroupChannelChattingViewController
extension ChatListViewController{
    
    func didStartReconnection() {
        
    }
    
    func didSucceedReconnection() {
        let query = SBDGroupChannel.createMyGroupChannelListQuery()
        query?.limit = 20
        query?.order = SBDGroupChannelListOrder.latestLastMessage
        query?.loadNextPage(completionHandler: { (channels, error) in
            if error != nil {
                return
            }
            
            self.channels.removeAll()
            self.channels.append(contentsOf: channels!)
            DispatchQueue.main.async {
                self.groupChannelListQuery = query
                self.tableView.reloadData()
            }
        })
    }
    
    func didFailReconnection() {
        
    }
    
}
    
// MARK: SBDChannelDelegate
extension ChatListViewController{
    
    func channel(_ sender: SBDBaseChannel, didReceive message: SBDBaseMessage) {
        if sender is SBDGroupChannel {
            let messageReceivedChannel = sender as! SBDGroupChannel
            if self.channels.index(of: messageReceivedChannel) != nil {
                self.channels.remove(at: self.channels.index(of: messageReceivedChannel)!)
            }
            self.channels.insert(messageReceivedChannel, at: 0)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func channelDidUpdateReadReceipt(_ sender: SBDGroupChannel) {
        
    }
    
    func channelDidUpdateTypingStatus(_ sender: SBDGroupChannel) {
        if self.editableChannel == true {
            return
        }
        
        let row = self.channels.index(of: sender)
        if row != nil {
            let cell = self.tableView.cellForRow(at: IndexPath(row: row!, section: 0)) as! ChatListTableViewCell
            
            cell.startTypingAnimation()
        }
    }
    
    func channel(_ sender: SBDGroupChannel, userDidJoin user: SBDUser) {
        DispatchQueue.main.async {
            if self.channels.index(of: sender) == nil {
                self.channels.append(sender)
            }
            self.tableView.reloadData()
        }
    }
    
    func channel(_ sender: SBDGroupChannel, userDidLeave user: SBDUser) {
        if user.userId == SBDMain.getCurrentUser()?.userId {
            if let index = self.channels.index(of: sender) {
                self.channels.remove(at: index)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func channel(_ sender: SBDOpenChannel, userDidEnter user: SBDUser) {
        
    }
    
    func channel(_ sender: SBDOpenChannel, userDidExit user: SBDUser) {
        
    }
    
    func channel(_ sender: SBDOpenChannel, userWasMuted user: SBDUser) {
        
    }
    
    func channel(_ sender: SBDOpenChannel, userWasUnmuted user: SBDUser) {
        
    }
    
    func channel(_ sender: SBDOpenChannel, userWasBanned user: SBDUser) {
        
    }
    
    func channel(_ sender: SBDOpenChannel, userWasUnbanned user: SBDUser) {
        
    }
    
    func channelWasFrozen(_ sender: SBDOpenChannel) {
        
    }
    
    func channelWasUnfrozen(_ sender: SBDOpenChannel) {
        
    }
    
    func channelWasChanged(_ sender: SBDBaseChannel) {
        if sender is SBDGroupChannel {
            let messageReceivedChannel = sender as! SBDGroupChannel
            if self.channels.index(of: messageReceivedChannel) != nil {
                self.channels.remove(at: self.channels.index(of: messageReceivedChannel)!)
            }
            self.channels.insert(messageReceivedChannel, at: 0)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func channelWasDeleted(_ channelUrl: String, channelType: SBDChannelType) {
        
    }
    
    func channel(_ sender: SBDBaseChannel, messageWasDeleted messageId: Int64) {
        
    }
    
}












