//
//  ConvoViewController.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK
import FirebaseDatabase
import RZTransitions

class ConvoViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{

    //MARK: Variables
    
    var dataRef: DatabaseReference!
    
    var data: [[String:AnyObject]] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navItem: UINavigationItem!
    
    fileprivate var refreshControl: UIRefreshControl?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
    
}

//MARK: ViewController Methods
extension ConvoViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RZTransitionsManager.shared().setAnimationController(RZZoomPushAnimationController(), fromViewController: type(of: self), for: .dismiss)
        
        dataRef = Database.database().reference()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refreshList), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        let negativeLeftSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeLeftSpacer.width = -2
        let negativeRightSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeRightSpacer.width = -2
        
        let leftCloseItem = UIBarButtonItem(image: UIImage(named: "btn_back"), style: UIBarButtonItemStyle.done, target: self, action: #selector(close))
        let rightCreateOpenChannelItem = UIBarButtonItem(image: UIImage(named: "btn_plus"), style: UIBarButtonItemStyle.done, target: self, action: #selector(createQuestion))
        
        self.navItem.leftBarButtonItems = [negativeLeftSpacer, leftCloseItem]
        self.navItem.rightBarButtonItems = [negativeRightSpacer, rightCreateOpenChannelItem]
        
        getData()
        
        tableView.register(Conversations.nib(), forCellReuseIdentifier: Conversations.cellReuseIdentifier())
        
    }
    
}

//MARK: Refresh Target Method
extension ConvoViewController{
    
    @objc fileprivate func refreshList() {
        
        data.removeAll()
        
        dataRef.child("Posts").observeSingleEvent(of: .value, with: { (snapshot) in
            let dict = snapshot.value as? NSDictionary
            
            if dict != nil {
                
                for firebaseData in dict! {
                    
                    let dict2:[String:AnyObject] = firebaseData.value as! [String:AnyObject]
                    self.data.append(dict2)
                    
                }
                
            }
            
            self.sortDataByDate()
            
            self.refreshControl?.endRefreshing()
            self.tableView.reloadData()
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
}

//MARK: Navigation Target Methods
extension ConvoViewController{
    
    @objc fileprivate func close() {
        self.transitioningDelegate = RZTransitionsManager.shared()
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc fileprivate func createQuestion(){
        
        let storyboard = UIStoryboard(name: "CreateConvoViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateConvoViewController")
        
        self.present(controller, animated: true, completion: nil)
        
    }
    
}
    
// MARK: UITableViewDelegate
extension ConvoViewController{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let starter: String = data[indexPath.row]["starter"] as! String
        
        if starter.characters.count > 60 {
            return 100
        }
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.loadFromNibWithName(nibNamed: "MeetHeaderView")
        return view
        
    }
    
}
    
// MARK: UITableViewDataSource
extension ConvoViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: Conversations?
        
        cell = tableView.dequeueReusableCell(withIdentifier: Conversations.cellReuseIdentifier()) as! Conversations?
        
        let dict = data[indexPath.row]
        
        let query = SBDMain.createUserListQuery(withUserIds: [data[indexPath.row]["userID"] as! String])
        
        cell?.setRow(row: indexPath.row)
        cell?.setConversation(subject: dict["subject"]! as! String, starter: dict["starter"]! as! String)
        
        query?.loadNextPage(completionHandler: { (users, error) in
            if error != nil {
                NSLog("Error: %@", error!)
                return
            }
            
            let user = users?.first
            
            cell?.checkIfOnline(user: user!)
            
        })
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let matchID:String = data[indexPath.row]["userID"] as! String
        
        if SBDMain.getCurrentUser()?.userId != matchID{
            
            SBDGroupChannel.createChannel(withName: data[indexPath.row]["subject"] as? String, isDistinct: false  , userIds: [matchID], coverUrl: nil, data: nil) { (channel, error) in
                
                if error != nil {
                    NSLog("Error: %@", error!)
                    return
                }
                
                tableView.deselectRow(at: indexPath, animated: false)
                
                self.dataRef.child("Posts").child(self.data[indexPath.row]["randomCode"] as! String).removeValue()
                
                let vc = ChatChattingViewController(nibName: "ChatChattingViewController", bundle: Bundle.main)
                vc.groupChannel = channel
                
                self.transitioningDelegate = RZTransitionsManager.shared()
                vc.transitioningDelegate = RZTransitionsManager.shared()
                
                self.present(vc, animated: true, completion: nil)
                
            }
            
        }else{
            
            let aC = UIAlertController.custom(title: "Error", message: "You can't message yourself", handler: nil)
            
            self.present(controller: aC)
            
            tableView.deselectRow(at: indexPath, animated: true)
            
        }
        
    }
    
}

//MARK: Miscellaneous
extension ConvoViewController{
    
    fileprivate func sortDataByDate(){
        
        self.data.sort {
            
            item1, item2 in
            let vote1 = (item1 as NSDictionary).value(forKey: "time") as! String
            let vote2 = (item2 as NSDictionary).value(forKey: "time") as! String
            return vote1 > vote2
            
        }
        
    }
    
    func getData(){
        
        dataRef.child("Posts").observeSingleEvent(of: .value, with: { (snapshot) in
            let dict = snapshot.value as? NSDictionary
            
            if dict != nil {
                
                for firebaseData in dict! {
                    
                    let dict2:[String:AnyObject] = firebaseData.value as! [String:AnyObject]
                    self.data.append(dict2)
                    
                }
                
            }
            
            self.sortDataByDate()
            
            self.tableView.reloadData()
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
}













