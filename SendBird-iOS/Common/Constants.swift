//
//  Constants.swift
//  Meetapp
//
//  Created by Jed Kyung on 10/17/16.
//  Copyright © 2016 SendBird. All rights reserved.
//

import UIKit

class Constants: NSObject {
    static func navigationBarTitleColor() -> UIColor {
        return UIColor(hexadecimal: 0xffffff)
    }
    
    static func navigationBarSubTitleColor() -> UIColor {
        return UIColor(hexadecimal: 0x00A99D)
    }
    
    static func navigationBarTitleFont() -> UIFont {
        return UIFont(name: "Roboto", size: 16.0)!
    }
    
    static func navigationBarSubTitleFont() -> UIFont {
        return UIFont(name: "Roboto-LightItalic", size: 10.0)!
    }
    
    static func textFieldLineColorNormal() -> UIColor {
        return UIColor(hexadecimal: 0xffffff)
    }
    
    static func textFieldLineColorSelected() -> UIColor {
        return UIColor(hexadecimal: 0x00A99D)
    }
    
    static func nicknameFontInMessage() -> UIFont {
        return UIFont(name: "Roboto", size: 12.0)!
    }
    
    static func nicknameColorInMessageNo0() -> UIColor {
        return UIColor(hexadecimal: 0x4f4f4f)
    }
    
    static func nicknameColorInMessageNo1() -> UIColor {
        return UIColor(hexadecimal: 0x4f4f4f)
    }
    
    static func nicknameColorInMessageNo2() -> UIColor {
        return UIColor(hexadecimal: 0x4f4f4f)
    }
    
    static func nicknameColorInMessageNo3() -> UIColor {
        return UIColor(hexadecimal: 0x4f4f4f)
    }
    
    static func nicknameColorInMessageNo4() -> UIColor {
        return UIColor(hexadecimal: 0x4f4f4f)
    }
    
    static func messageDateFont() -> UIFont {
        return UIFont(name: "Roboto", size: 10.0)!
    }
    
    static func messageDateColor() -> UIColor {
        return UIColor(red: 191.0/255.0, green: 191.0/255.0, blue: 191.0/255.0, alpha: 1)
    }
    
    static func incomingFileImagePlaceholderColor() -> UIColor {
        return UIColor(red: 238.0/255.0, green: 241.0/255.0, blue: 246.0/255.0, alpha: 1)
    }
    
    static func messageFont() -> UIFont {
        return UIFont(name: "Roboto", size: 16.0)!
    }
    
    static func outgoingMessageColor() -> UIColor {
        return UIColor.black
    }
    
    static func incomingMessageColor() -> UIColor {
        return UIColor.black
    }

    static func outgoingFileImagePlaceholderColor() -> UIColor {
        return UIColor(red: 128.0/255.0, green: 90.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    static func openChannelLineColorNo0() -> UIColor {
        return UIColor(red: 45.0/255.0, green: 227.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    static func openChannelLineColorNo1() -> UIColor {
        return UIColor(red: 53.0/255.0, green: 163.0/255.0, blue: 251.0/255.0, alpha: 1)
    }
    
    static func openChannelLineColorNo2() -> UIColor {
        return UIColor(red: 128.0/255.0, green: 90.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    static func openChannelLineColorNo3() -> UIColor {
        return UIColor(red: 207.0/255.0, green: 72.0/255.0, blue: 251.0/255.0, alpha: 1)
    }
    
    static func openChannelLineColorNo4() -> UIColor {
        return UIColor(red: 226.0/255.0, green: 72.0/255.0, blue: 195.0/255.0, alpha: 1)
    }
    
    //Meet colors
    
    static func meetChannelLineColorNo0() -> UIColor {
        return UIColor(hexadecimal: 0xfff200)
        
    }
    
    static func meetChannelLineColorNo1() -> UIColor {
        return UIColor(hexadecimal: 0xffbb00)
        
    }
    
    static func meetChannelLineColorNo2() -> UIColor {
        return UIColor(hexadecimal: 0xff7b00)
        
    }
    
    static func meetChannelLineColorNo3() -> UIColor {
        return UIColor(hexadecimal: 0xff4300)
        
    }
    
    static func meetChannelLineColorNo4() -> UIColor {
        return UIColor(hexadecimal: 0xff0000)
        
    }
    
    static func leaveButtonColor() -> UIColor {
        return UIColor.red
    }
    
    static func hideButtonColor() -> UIColor {
        return UIColor(red: 116.0/255.0, green: 127.0/255.0, blue: 145.0/255.0, alpha: 1)
    }
    
    static func leaveButtonFont() -> UIFont {
        return UIFont(name: "Roboto", size: 16.0)!
    }
    
    static func hideButtonFont() -> UIFont {
        return UIFont(name: "Roboto", size: 16.0)!
    }
    
    static func distinctButtonSelected() -> UIFont {
        return UIFont(name: "Roboto-Medium", size: 18.0)!
    }
    
    static func distinctButtonNormal() -> UIFont {
        return UIFont(name: "Roboto", size: 18.0)!
    }
    
    static func navigationBarButtonItemFont() -> UIFont {
        return UIFont(name: "Roboto", size: 16.0)!
    }
    
    static func memberOnlineTextColor() -> UIColor {
        return UIColor(red: 41.0/255.0, green: 197.0/255.0, blue: 25.0/255.0, alpha: 1)
    }
    
    static func memberOfflineDateTextColor() -> UIColor {
        return UIColor(red: 142.0/255.0, green: 142.0/255.0, blue: 142.0/255.0, alpha: 1)
    }
    
    static func connectButtonColor() -> UIColor {
        return UIColor(hexadecimal: 0x00A99D)
    }
    
    static func urlPreviewDescriptionFont() -> UIFont {
        return UIFont(name: "Roboto-Light", size: 12.0)!
    }
}
