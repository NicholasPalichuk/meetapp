//
//  CreateConvoViewController.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK
import FirebaseDatabase
import RZTransitions

class CreateConvoViewController: UIViewController,UITextFieldDelegate {

    //MARK: Variables
    
    var dataRef: DatabaseReference!
    
    @IBOutlet weak var subjectLine: UIView!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var margin1: NSLayoutConstraint!
    
    @IBOutlet weak var starterLine: UIView!
    @IBOutlet weak var starterLabel: UILabel!
    @IBOutlet weak var starterTextField: UITextField!
    @IBOutlet weak var margin2: NSLayoutConstraint!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
    
}

//MARK: ViewController Methods
extension CreateConvoViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataRef = Database.database().reference()
                
        self.subjectLabel.alpha = 0
        self.subjectTextField.delegate = self
        self.subjectTextField.addTarget(self, action: #selector(channelNameTextFieldDidChange(sender:)), for: UIControlEvents.editingChanged)
        
        self.starterLabel.alpha = 0
        self.starterTextField.delegate = self
        self.starterTextField.addTarget(self, action: #selector(channelNameTextFieldDidChange(sender:)), for: UIControlEvents.editingChanged)
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
}

//MARK: UITextField Target Methods
extension CreateConvoViewController{
    
    func channelNameTextFieldDidChange(sender: UITextField) {
        
        if sender == subjectTextField{
            
            if sender.text?.characters.count == 0 {
                self.margin1.constant = -12
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: 0.1, animations: {
                    self.subjectLabel.alpha = 0
                    self.view.layoutIfNeeded()
                })
            }
            else {
                self.margin1.constant = 0
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: 0.2, animations: {
                    self.subjectLabel.alpha = 1
                    self.view.layoutIfNeeded()
                })
            }
            
        }else{
            
            if sender.text?.characters.count == 0 {
                self.margin2.constant = -12
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: 0.1, animations: {
                    self.starterLabel.alpha = 0
                    self.view.layoutIfNeeded()
                })
            }
            else {
                self.margin2.constant = 0
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: 0.2, animations: {
                    self.starterLabel.alpha = 1
                    self.view.layoutIfNeeded()
                })
            }
            
        }
        
    }
    
}

//MARK: Navigation Bar Target Methods
extension CreateConvoViewController{
    
    @IBAction fileprivate func close() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction fileprivate func submitConvo(){
        
        if subjectTextField.text?.characters.count == 0{
            
            let aC = UIAlertController.custom(title: "Subject Field is Empty", message: "You must put something in the subject field", handler: nil)
            self.present(controller: aC)
            
            return
            
        }
        
        if starterTextField.text?.characters.count == 0{
            
            let aC = UIAlertController.custom(title: "Starter Field is Empty", message: "You must put something in the starter field", handler: nil)
            self.present(controller: aC)
            
            return
            
        }
        
        let randomCodeRef = dataRef.childByAutoId()
        let randomCode = randomCodeRef.key
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "h:mm MMMdd/yy"
        
        let date = Date()
        
        dataRef.child("Posts").child(randomCode).child("subject").setValue(subjectTextField.text!)
        dataRef.child("Posts").child(randomCode).child("starter").setValue(starterTextField.text!)
        dataRef.child("Posts").child(randomCode).child("randomCode").setValue(randomCode)
        dataRef.child("Posts").child(randomCode).child("userID").setValue(SBDMain.getCurrentUser()?.userId)
        dataRef.child("Posts").child(randomCode).child("time").setValue(dateFormatterGet.string(from: date))
        dataRef.child("Posts").child(randomCode).child("postTime").setValue(dateFormatterPrint.string(from: date))
        
        print(dateFormatterPrint.string(from: date))
        
        self.dismiss(animated: true) {}
        
    }
    
}

//MARK: UITextFieldDelegate
extension CreateConvoViewController{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.subjectTextField {
            
            self.subjectLine.backgroundColor = Constants.textFieldLineColorSelected()
            self.starterLine.backgroundColor = Constants.textFieldLineColorNormal()
            
        }else{
            
            self.subjectLine.backgroundColor = Constants.textFieldLineColorNormal()
            self.starterLine.backgroundColor = Constants.textFieldLineColorSelected()
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.subjectTextField {
            self.subjectLine.backgroundColor = Constants.textFieldLineColorNormal()
            
        }else{
            self.starterLine.backgroundColor = Constants.textFieldLineColorNormal()
            
        }
    }
}

















