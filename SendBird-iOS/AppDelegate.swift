//
//  AppDelegate.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import UserNotifications
import AudioToolbox
import SendBirdSDK
import AVKit
import AudioToolbox
import AVFoundation
import Firebase
import RZTransitions
import NotificationBannerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate , SBDChannelDelegate {

    var window: UIWindow?
    var receivedPushChannelUrl: String?
    
    fileprivate var groupChannelListQuery: SBDGroupChannelListQuery?
    
    static let instance: NSCache<AnyObject, AnyObject> = NSCache()

    static func imageCache() -> NSCache<AnyObject, AnyObject>! {
        if AppDelegate.instance.totalCostLimit == 104857600 {
            AppDelegate.instance.totalCostLimit = 104857600
        }
        
        return AppDelegate.instance
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        SBDMain.add(self as SBDChannelDelegate, identifier: self.description)
        
        UIApplication.shared.registerForRemoteNotifications()
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        center.requestAuthorization(options: [.badge, .sound, .alert]) { (granted, error) in
            
            if (granted) {
                
            } else{
                print("Notification permissions not granted")
            }
            
        }
        
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: Constants.navigationBarTitleFont()]
        UINavigationBar.appearance().tintColor = Constants.navigationBarTitleColor()
        
        application.applicationIconBadgeNumber = 0
        
        RZTransitionsManager.shared().defaultPresentDismissAnimationController = RZCardSlideAnimationController()
        RZTransitionsManager.shared().defaultPushPopAnimationController = RZCardSlideAnimationController()
        
        FirebaseApp.configure()
        
        SBDMain.initWithApplicationId("BF8A81E2-0575-436E-8EAA-0557C669A1A4")
        SBDMain.setLogLevel(SBDLogLevel.none)
        SBDOptions.setUseMemberAsMessageSender(true)
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
        
        }
        
        return true
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive: UNNotificationResponse,
                                withCompletionHandler: @escaping ()->()) {
        withCompletionHandler()
        let userInfo = didReceive.notification.request.content.userInfo
        
        if userInfo["sendbird"] != nil {
            let sendBirdPayload = userInfo["sendbird"] as! Dictionary<String, Any>
            let channel = (sendBirdPayload["channel"]  as! Dictionary<String, Any>)["channel_url"] as! String
            
            self.receivedPushChannelUrl = channel
            
            print("stuff")
            
            bannerTarget(channel)
            
        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        SBDMain.registerDevicePushToken(deviceToken, unique: true) { (status, error) in
            if error == nil {
                if status == SBDPushTokenRegistrationStatus.pending {
                    
                }
                else {
                    
                }
            }
            else {
                
            }
        }
    }
    
    func channel(_ sender: SBDBaseChannel, didReceive message: SBDBaseMessage) {
        
        let topView = UIApplication.topViewController()
        
        if let view = topView as? ChatChattingViewController{
            
            if view.groupChannel.channelUrl != sender.channelUrl{
                
                if sender is SBDGroupChannel {
                    
                    self.receivedPushChannelUrl = sender.channelUrl
                    
                    let banner = NotificationBanner(title: "sure222", subtitle: "sure", style: .info)
                    let generator = UINotificationFeedbackGenerator()
                    generator.notificationOccurred(.success)
                    banner.onTap = {self.bannerTarget(sender.channelUrl)}
                    banner.show()
                    
                }
            }
            
        }else{
            
            if sender is SBDGroupChannel {
                
                self.receivedPushChannelUrl = sender.channelUrl
                
                let banner = NotificationBanner(title: "sure222", subtitle: "sure", style: .info)
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.success)
                banner.onTap = {self.bannerTarget(sender.channelUrl)}
                banner.show()
                
            }
            
        }
    }
    
    fileprivate func bannerTarget(_ channel: String){
        
        print("stuff")
        
        let topView = UIApplication.topViewController()
        
        if let view = topView as? ChatChattingViewController{
            
            if view.groupChannel.channelUrl != channel{
                
                view.dismiss(animated: false, completion: { 
                    
                    let newTopView = UIApplication.topViewController()
                    
                    let vc = ChatChattingViewController(nibName: "ChatChattingViewController", bundle: Bundle.main)
                    SBDGroupChannel.getWithUrl(channel, completionHandler: { (channel, error) in
                        
                        vc.groupChannel = channel!
                        
                        newTopView?.transitioningDelegate = RZTransitionsManager.shared()
                        vc.transitioningDelegate = RZTransitionsManager.shared()
                        newTopView?.present(controller: vc)
                        
                    })
                    
                })
                
            }
            
        }else{
            
            let vc = ChatChattingViewController(nibName: "ChatChattingViewController", bundle: Bundle.main)
            SBDGroupChannel.getWithUrl(channel, completionHandler: { (channel, error) in
                
                vc.groupChannel = channel!
                
                topView?.transitioningDelegate = RZTransitionsManager.shared()
                vc.transitioningDelegate = RZTransitionsManager.shared()
                topView?.present(controller: vc)
                
            })
            
        }
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        if SBDMain.getCurrentUser() != nil{
            
            self.groupChannelListQuery = SBDGroupChannel.createMyGroupChannelListQuery()
            self.groupChannelListQuery?.limit = 20
            self.groupChannelListQuery?.order = SBDGroupChannelListOrder.latestLastMessage
            
            var count = 0
            
            self.groupChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
                if error != nil {
                    return
                    
                }
                
                for channel in channels! {
                    count += Int(channel.unreadMessageCount)
                    
                }
                
                application.applicationIconBadgeNumber = count
                
            })
            
        }
        
    }
    
}

