//
//  ParticipantListViewController.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK

class ParticipantListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    
    var openChannel: SBDOpenChannel!
    
    fileprivate var query: SBDUserListQuery?
    fileprivate var participants: [SBDUser] = []
    fileprivate var refreshControl: UIRefreshControl?
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}

}

//MARK: UIViewController Methods
extension ParticipantListViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(ParticipantListTableViewCell.nib(), forCellReuseIdentifier: ParticipantListTableViewCell.cellReuseIdentifier())
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refreshList), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        let negativeLeftSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeLeftSpacer.width = -2
        let leftCloseItem = UIBarButtonItem(image: UIImage(named: "btn_close"), style: UIBarButtonItemStyle.done, target: self, action: #selector(close))
        self.navItem.leftBarButtonItems = [negativeLeftSpacer, leftCloseItem]
        
        self.loadList(initial: true)
    }
    
}

//MARK: Refresh Target Method
extension ParticipantListViewController{
    
    @objc fileprivate func refreshList() {
        self.loadList(initial: true)
    }
    
    fileprivate func loadList(initial: Bool) {
        if initial {
            self.participants.removeAll()
            self.query = self.openChannel.createParticipantListQuery()
        }
        
        if self.query?.hasNext == false {
            return
        }
        
        self.query?.loadNextPage(completionHandler: { (users, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.refreshControl?.endRefreshing()
                }
                
                return
            }
            
            for participant in users! {
                self.participants.append(participant)
            }
            
            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
            
        })
    }
    
}

//MARK: Target Methods
extension ParticipantListViewController{
    
    @objc fileprivate func close() {
        self.dismiss(animated: false, completion: nil)
    }
    
}

// MARK: UITableViewDelegate
extension ParticipantListViewController{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
}
    
// MARK: UITableViewDataSource
extension ParticipantListViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.participants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ParticipantListTableViewCell = tableView.dequeueReusableCell(withIdentifier: ParticipantListTableViewCell.cellReuseIdentifier()) as! ParticipantListTableViewCell
        
        cell.setModel(aUser: self.participants[indexPath.row])
        
        if self.participants.count > 0 && indexPath.row + 1 == self.participants.count {
            self.loadList(initial: false)
        }
        
        return cell
    }
    
}
