//
//  CreateGroupViewController.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK

protocol CreateGroupChatViewControllerDelegate: class {
    func refreshView(vc: UIViewController)
}

class CreateGroupChatViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: Variables
    
    weak var delegate: CreateGroupChatViewControllerDelegate?
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var channelNameLabel: UILabel!
    @IBOutlet weak var openChannelNameTextField: UITextField!
    @IBOutlet weak var channelNameLabelBottomMargin: NSLayoutConstraint!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
    
}

//MARK: UIViewController Methods
extension CreateGroupChatViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.channelNameLabel.alpha = 0
        self.openChannelNameTextField.delegate = self
        self.openChannelNameTextField.addTarget(self, action: #selector(channelNameTextFieldDidChange(sender:)), for: UIControlEvents.editingChanged)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        openChannelNameTextField.resignFirstResponder()
        
    }
    
}

//MARK: Target Methods
extension CreateGroupChatViewController{
    
    @IBAction fileprivate func closeViewController() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction fileprivate func createOpenChannel() {
        if self.openChannelNameTextField.text?.characters.count == 0 {
            return
        }
        
        SBDOpenChannel.createChannel(withName: self.openChannelNameTextField.text, coverUrl: nil, data: nil, operatorUsers: nil) { (channel, error) in
            if error != nil {
                let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                vc.addAction(closeAction)
                DispatchQueue.main.async {
                    self.present(vc, animated: true, completion: nil)
                }
                
                return
            }
            
            self.delegate?.refreshView(vc: self)
            let vc = UIAlertController(title: "Done!", message: "You made a group", preferredStyle: .alert)
            let closeAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { (action) in
                self.dismiss(animated: false, completion: nil)
            })
            vc.addAction(closeAction)
            DispatchQueue.main.async {
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
}
    
// MARK: UITextFieldDelegate
extension CreateGroupChatViewController{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.openChannelNameTextField {
            self.lineView.backgroundColor = Constants.textFieldLineColorSelected()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.openChannelNameTextField {
            self.lineView.backgroundColor = Constants.textFieldLineColorNormal()
        }
    }
    
}

//MARK: Miscellaneous
extension CreateGroupChatViewController{
    
    func channelNameTextFieldDidChange(sender: UITextField) {
        if sender.text?.characters.count == 0 {
            self.channelNameLabelBottomMargin.constant = -12
            self.view.setNeedsUpdateConstraints()
            UIView.animate(withDuration: 0.1, animations: {
                self.channelNameLabel.alpha = 0
                self.view.layoutIfNeeded()
            })
        }
        else {
            self.channelNameLabelBottomMargin.constant = 0
            self.view.setNeedsUpdateConstraints()
            UIView.animate(withDuration: 0.2, animations: {
                self.channelNameLabel.alpha = 1
                self.view.layoutIfNeeded()
            })
        }
    }
    
}










