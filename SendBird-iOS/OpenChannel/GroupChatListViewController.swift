//
//  GroupChatListViewController.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK
import RZTransitions

class GroupChatListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CreateGroupChatViewControllerDelegate {
    
    //MARK: Variables
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var refreshControl: UIRefreshControl?
    fileprivate var channels: [SBDOpenChannel] = []
    fileprivate var openChannelListQuery: SBDOpenChannelListQuery?

    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
    
}

//MARK: UIViewController Methods
extension GroupChatListViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(GroupChatListTableViewCell.nib(), forCellReuseIdentifier: GroupChatListTableViewCell.cellReuseIdentifier())
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refreshChannelList), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        let negativeLeftSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeLeftSpacer.width = -2
        let negativeRightSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeRightSpacer.width = -2
        
        let leftBackItem = UIBarButtonItem(image: UIImage(named: "btn_back"), style: UIBarButtonItemStyle.done, target: self, action: #selector(back))
        let rightCreateOpenChannelItem = UIBarButtonItem(image: UIImage(named: "btn_plus"), style: UIBarButtonItemStyle.done, target: self, action: #selector(createOpenChannel))
        
        self.navItem.leftBarButtonItems = [negativeLeftSpacer, leftBackItem]
        self.navItem.rightBarButtonItems = [negativeRightSpacer, rightCreateOpenChannelItem]
        
        self.refreshChannelList()
    }
    
}

//MARK: Refresh Target Method
extension GroupChatListViewController{
    
    @objc fileprivate func refreshChannelList() {
        self.channels.removeAll()
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        self.openChannelListQuery = SBDOpenChannel.createOpenChannelListQuery()
        self.openChannelListQuery?.limit = 20
        
        self.openChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.refreshControl?.endRefreshing()
                }
                
                let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                vc.addAction(closeAction)
                DispatchQueue.main.async {
                    self.present(vc, animated: true, completion: nil)
                }
                
                return
            }
            
            for channel in channels! {
                self.channels.append(channel)
            }
            
            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        })
    }
    
}

//MARK: Target Methods
extension GroupChatListViewController{
    
    @objc fileprivate func back() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc fileprivate func createOpenChannel() {
        let vc = CreateGroupChatViewController(nibName: "CreateGroupChatViewController", bundle: Bundle.main)
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}

// MARK: CreateOpenChannelViewControllerDelegate
extension GroupChatListViewController{
    
    func refreshView(vc: UIViewController) {
        self.refreshChannelList()
    }
    
}


// MARK: UITableViewDelegate
extension GroupChatListViewController{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        self.channels[indexPath.row].enter { (error) in
            if error != nil {
                let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                vc.addAction(closeAction)
                DispatchQueue.main.async {
                    self.present(vc, animated: true, completion: nil)
                }
                
                return
            }
            
            let vc = GroupChatChattingViewController(nibName: "GroupChatChattingViewController", bundle: Bundle.main)
            vc.openChannel = self.channels[indexPath.row]
            DispatchQueue.main.async {
                
                self.transitioningDelegate = RZTransitionsManager.shared()
                vc.transitioningDelegate = RZTransitionsManager.shared()
                
                self.present(vc, animated: true, completion: nil)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.loadFromNibWithName(nibNamed: "GroupHeaderView")
        return view
    }
    
}


// MARK: UITableViewDataSource
extension GroupChatListViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GroupChatListTableViewCell.cellReuseIdentifier())
        
        (cell as! GroupChatListTableViewCell).setModel(aChannel: self.channels[indexPath.row])
        (cell as! GroupChatListTableViewCell).setRow(row: indexPath.row)
        
        if self.channels.count > 0 && indexPath.row + 1 == self.channels.count {
            self.loadChannels()
        }
        
        return cell!
    }
    
}

//MARK: Miscellaneous
extension GroupChatListViewController{
    
    fileprivate func loadChannels() {
        if self.openChannelListQuery?.hasNext == false {
            return
        }
        
        self.openChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
            if error != nil {
                let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                vc.addAction(closeAction)
                DispatchQueue.main.async {
                    self.present(vc, animated: true, completion: nil)
                }
                
                return
            }
            
            for channel in channels! {
                self.channels.append(channel)
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
}

    













