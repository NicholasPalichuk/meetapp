//
//  BlockedUserListTableViewCell.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK
import AlamofireImage

class BlockedUserListTableViewCell: UITableViewCell {
    
    //MARK: Variables
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nicknameLabel: UILabel!
    
    fileprivate var user: SBDUser!
    
}

//MARK: UITableViewCell Methods
extension BlockedUserListTableViewCell{
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    static func cellReuseIdentifier() -> String {
        return String(describing: self)
    }
    
}

//MARK: Set Methods
extension BlockedUserListTableViewCell{
    
    func setModel(aUser: SBDUser) {
        self.user = aUser
        
        self.profileImageView.af_setImage(withURL: URL(string: self.user.profileUrl!)!, placeholderImage: UIImage(named: "img_profile"))
        self.nicknameLabel.text = self.user.nickname
    }
    
}
