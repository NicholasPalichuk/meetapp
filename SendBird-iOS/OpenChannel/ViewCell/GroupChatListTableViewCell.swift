//
//  GroupChatListTableViewCell.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK

class GroupChatListTableViewCell: UITableViewCell {
    
    //MARK: Variables
    
    @IBOutlet weak var leftLineView: UIView!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var participantCountLabel: UILabel!
    
    fileprivate var channel: SBDOpenChannel!

}

//MARK: UITableViewCell Methods
extension GroupChatListTableViewCell{
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    static func cellReuseIdentifier() -> String {
        return String(describing: self)
    }
    
}

//MARK: Set Methods
extension GroupChatListTableViewCell{
    
    func setRow(row: NSInteger) {
        switch (row % 5) {
        case 0:
            self.leftLineView.backgroundColor = Constants.openChannelLineColorNo0()
            break;
            
        case 1:
            self.leftLineView.backgroundColor = Constants.openChannelLineColorNo1()
            break;
            
        case 2:
            self.leftLineView.backgroundColor = Constants.openChannelLineColorNo2()
            break;
            
        case 3:
            self.leftLineView.backgroundColor = Constants.openChannelLineColorNo3()
            break;
            
        case 4:
            self.leftLineView.backgroundColor = Constants.openChannelLineColorNo4()
            break;
            
        default:
            self.leftLineView.backgroundColor = Constants.openChannelLineColorNo0()
            break;
        }
    }
    
    func setModel(aChannel: SBDOpenChannel) {
        
        self.channel = aChannel
        
        self.channelName.text = self.channel.name
        self.participantCountLabel.text = String(format: "%d Online", aChannel.participantCount)
        
    }
    
}

