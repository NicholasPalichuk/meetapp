//
//  MessageDelegate.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 10/6/16.
//  Copyright © 2017 Meetapp. All rights reserved.
//

import Foundation
import UIKit
import SendBirdSDK

protocol MessageDelegate: class {
    func clickProfileImage(viewCell: UITableViewCell, user: SBDUser)
    func clickMessage(view: UIView, message: SBDBaseMessage)
    func clickResend(view: UIView, message: SBDBaseMessage)
    func clickDelete(view: UIView, message: SBDBaseMessage)
}
