//
//  LoginController.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK
import FirebaseDatabase

class LoginController: UIViewController, UITextFieldDelegate {
    
    //MARK: Variables
    
    var dataRef: DatabaseReference!
    
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var userIdTextField: UITextField!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var userIdLabelBottomMargin: NSLayoutConstraint!

    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
    
}

//MARK: UITableViewController Methods
extension LoginController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataRef = Database.database().reference()
        
        // Version
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")
        if path != nil {
            let infoDict = NSDictionary(contentsOfFile: path!)
            let sampleUIVersion = infoDict?["CFBundleShortVersionString"] as! String
            _ = String(format: "v%@", sampleUIVersion)
            //self.versionLabel.text = version
        }
        
        userIdTextField.setLeftPaddingPoints(20)
        
        self.userIdTextField.delegate = self
        
        self.userIdLabel.alpha = 0
        
        let userId = UserDefaults.standard.object(forKey: "userID") as? String
        
        if userId != nil && (userId?.characters.count)! > 0 {
            self.userIdLabelBottomMargin.constant = 0
            self.view.setNeedsUpdateConstraints()
            self.userIdLabel.alpha = 1
            self.view.layoutIfNeeded()
        }
        
        self.userIdTextField.text = userId
        
        self.indicatorView.hidesWhenStopped = true
        
        self.userIdTextField.addTarget(self, action: #selector(userIdTextFieldDidChange(sender:)), for: UIControlEvents.editingChanged)
        
        if userId != nil && (userId?.characters.count)! > 0 {
            self.connect()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        userIdTextField.resignFirstResponder()
        
    }
    
}

//MARK: Connect Methods
extension LoginController{
    
    @IBAction func clickConnectButton(_ sender: AnyObject) {
        clickConnectButtonAction()
        
    }
    
    func clickConnectButtonAction(){
        
        guard (self.userIdTextField.text?.characters.count)! < 12 else {
            
            let aC = UIAlertController.custom(title: "Name", message: "You nickname must be under 12 characters", handler: nil)
            self.present(controller: aC)
            
            return
            
        }
        
        guard (self.userIdTextField.text?.characters.count)! > 3 else {
            
            let aC = UIAlertController.custom(title: "Name", message: "Your nickname must be atleast 3 characters", handler: nil)
            self.present(controller: aC)
            
            return
            
        }
        
        var isEmojiOrSpace = false
        
        for char in (userIdTextField.text?.characters)!{
            
            if char == " "{
             
                isEmojiOrSpace = true
                
                break
                
            }
            
            let str = "\(char)"
            
            if str.isSingleEmoji{
                
                isEmojiOrSpace = true
                
                break
                
            }
            
        }
        
        guard !isEmojiOrSpace else {
            
            let aC = UIAlertController.custom(title: "Name", message: "You nickname can't contain spaces or emojis", handler: nil)
            self.present(controller: aC)
            
            return
            
        }
            
        let trimmedUserId: String = (self.userIdTextField.text?.trimmingCharacters(in: NSCharacterSet.whitespaces))!
        
        self.indicatorView.startAnimating()
        connectButton.isEnabled = false
        
        dataRef.child("Users").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChild(trimmedUserId){
                
                self.indicatorView.stopAnimating()
                
                let aC = UIAlertController.custom(title: "Taken", message: "That name has already been taken.", handler: nil)
                self.present(controller: aC)
                
            }else{
                
                self.dataRef.child("Users").child(trimmedUserId).setValue(true)
                self.connect()
                
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    func connect() {
        let trimmedUserId: String = (self.userIdTextField.text?.trimmingCharacters(in: NSCharacterSet.whitespaces))!
        
        if trimmedUserId.characters.count > 0 {
            self.userIdTextField.isEnabled = false
            self.connectButton.isEnabled = false
            
            self.indicatorView.stopAnimating()
            self.indicatorView.startAnimating()
            
            SBDMain.connect(withUserId: trimmedUserId, completionHandler: { (user, error) in
                if error != nil {
                    DispatchQueue.main.async {
                        self.userIdTextField.isEnabled = true
                        self.connectButton.isEnabled = true
                        
                        self.indicatorView.stopAnimating()
                    }
                    
                    let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                    let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                    vc.addAction(closeAction)
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                SBDMain.updateCurrentUserInfo(withNickname: trimmedUserId, profileUrl: nil, completionHandler: { (error) in
                    
                    if error != nil{
                        
                        print(error.debugDescription)
                        
                    }
                    
                })
                
                if SBDMain.getPendingPushToken() != nil {
                    SBDMain.registerDevicePushToken(SBDMain.getPendingPushToken()!, unique: true, completionHandler: { (status, error) in
                        if error == nil {
                            if status == SBDPushTokenRegistrationStatus.pending {
                                print("Push registeration is pending.")
                            }
                            else {
                                print("APNS Token is registered.")
                            }
                        }
                        else {
                            print("APNS registration failed.")
                        }
                    })
                }
                
                UserDefaults.standard.set(trimmedUserId, forKey: "userID")
                
                DispatchQueue.main.async {
                    let vc = MenuViewController(nibName: "MenuViewController", bundle: Bundle.main)
                    self.present(vc, animated: false, completion: nil)
                }
            })
        }
    }
    
}

//MARK: TextField Target Methods
extension LoginController{
    
    func userIdTextFieldDidChange(sender: UITextField) {
        if sender.text?.characters.count == 0 {
            self.userIdLabelBottomMargin.constant = 12
            self.view.setNeedsUpdateConstraints()
            UIView.animate(withDuration: 0.1, animations: {
                self.userIdLabel.alpha = 0
                self.view.layoutIfNeeded()
            })
        }
        else {
            self.userIdLabelBottomMargin.constant = 0
            self.view.setNeedsUpdateConstraints()
            UIView.animate(withDuration: 0.2, animations: {
                self.userIdLabel.alpha = 1
                self.view.layoutIfNeeded()
            })
        }
    }
    
}

// MARK: UITextFieldDelegate
extension LoginController{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        clickConnectButtonAction()
        return true
        
    }
    
}









