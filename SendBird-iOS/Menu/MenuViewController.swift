//
//  MenuViewController.swift
//  Meetapp
//
//  Created by Nicholas Palichuk on 2017-06-29.
//  Copyright © 2017 Sevenapp. All rights reserved.
//

import UIKit
import SendBirdSDK
import RZTransitions

class MenuViewController: UIViewController, SBDConnectionDelegate {
    
    //MARK: Variables
    
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var groupChatView: UIView!

    var chatListViewController: ChatListViewController?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
    
}

//MARK: UIViewController Methods
extension MenuViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RZTransitionsManager.shared().setAnimationController(RZZoomPushAnimationController(), fromViewController: type(of: self), for: .presentDismiss)
        
        profileName.text = UserDefaults.standard.object(forKey: "userID") as? String
        
        let imgUrl = (SBDMain.getCurrentUser()?.profileUrl)
        profileImg.downloadedFrom(link: imgUrl!)
        
        SBDMain.add(self as SBDConnectionDelegate, identifier: self.description)
        
//        if (UIApplication.shared.delegate as! AppDelegate).receivedPushChannelUrl != nil {
//            let channelUrl = (UIApplication.shared.delegate as! AppDelegate).receivedPushChannelUrl
//            if channelUrl != nil {
//                SBDGroupChannel.getWithUrl(channelUrl!, completionHandler: { (channel, error) in
//                    let vc = ChatChattingViewController()
//                    vc.groupChannel = channel
//                    DispatchQueue.main.async {
//                        self.present(vc, animated: false, completion: nil)
//                    }
//                })
//            }
//        }
    }
    
}

//MARK: Actions
extension MenuViewController{
    
    @IBAction func pressOpenChannelButton(_ sender: AnyObject) {
//        self.openChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
//        self.groupChannelView.backgroundColor = UIColor.white
        
    }
    
    @IBAction func clickOpenChannelButton(_ sender: AnyObject) {
//        self.openChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
//        self.groupChannelView.backgroundColor = UIColor.white
        
        let vc = GroupChatListViewController(nibName: "GroupChatListViewController", bundle: Bundle.main)
        
        self.transitioningDelegate = RZTransitionsManager.shared()
        vc.transitioningDelegate = RZTransitionsManager.shared()
        
        self.present(vc, animated: true) {
            vc.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    @IBAction func pressGroupChannelButton(_ sender: AnyObject) {
//        self.openChannelView.backgroundColor = UIColor.white
//        self.groupChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
        
    }
    
    @IBAction func clickGroupChannelButton(_ sender: AnyObject) {
        self.showGroupChannelList()
    }
    
    @IBAction func clickConversationsButton(_ sender: Any) {
        
        let vc = ConvoViewController(nibName: "ConvoViewController", bundle: Bundle.main)
        //vc.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        RZTransitionsManager.shared().setAnimationController(RZZoomPushAnimationController(), fromViewController: type(of: vc.self), for: .dismiss)
        
        self.transitioningDelegate = RZTransitionsManager.shared()
        vc.transitioningDelegate = RZTransitionsManager.shared()
        
        self.present(vc, animated: true, completion: nil)
        
    }
    
}

//MARK: Miscellaneous
extension MenuViewController{
    
    fileprivate func showGroupChannelList() {
        
        if self.chatListViewController == nil {
            
            let storyboard = UIStoryboard(name: "ChatListViewController", bundle: nil)
            self.chatListViewController = storyboard.instantiateViewController(withIdentifier: "ChatListViewController") as? ChatListViewController
            
            //self.chatListViewController = ChatListViewController(nibName: "ChatListViewController", bundle: Bundle.main)
            self.chatListViewController?.addDelegates()
        }
        
        self.transitioningDelegate = RZTransitionsManager.shared()
        self.chatListViewController?.transitioningDelegate = RZTransitionsManager.shared()
        
        self.present(self.chatListViewController!, animated: true) {
            self.chatListViewController?.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
}
    
// MARK: GroupChannelChattingViewController
extension MenuViewController{
    
    func didStartReconnection() {
        
    }
    
    func didSucceedReconnection() {
        if (UIApplication.shared.delegate as! AppDelegate).receivedPushChannelUrl != nil {
            let channelUrl = (UIApplication.shared.delegate as! AppDelegate).receivedPushChannelUrl
            
            var topViewController = UIApplication.shared.keyWindow?.rootViewController
            while ((topViewController?.presentedViewController) != nil) {
                topViewController = topViewController?.presentedViewController
            }
            
            if topViewController is ChatChattingViewController {
                if (topViewController as! ChatChattingViewController).groupChannel.channelUrl == channelUrl {
                    return
                }
            }
            
//            ((UIApplication.shared).delegate as! AppDelegate).receivedPushChannelUrl = nil
//            SBDGroupChannel.getWithUrl(channelUrl!, completionHandler: { (channel, error) in
//                let vc = ChatChattingViewController()
//                vc.groupChannel = channel
//                DispatchQueue.main.async {
//                    topViewController?.present(vc, animated: false, completion: nil)
//                }
//            })
        }
    }
    
    func didFailReconnection() {
        
    }
    
}

//MARK: UNUSED BUT USEFUL CODE LATER (MAYBE)
//    func disconnect() {
//        SBDMain.unregisterAllPushToken { (response, error) in
//            if error != nil {
//                print("Unregister all push tokens. Error: %@", error!)
//            }
//
//            SBDMain.disconnect(completionHandler: {
//                DispatchQueue.main.async {
//                    UIApplication.shared.applicationIconBadgeNumber = 0
//                    self.dismiss(animated: false, completion: nil)
//                }
//            })
//        }
//    }
